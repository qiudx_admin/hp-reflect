package com.kvn.reflect;

import java.lang.reflect.Method;

/**
 * 从测试结果来看，jdk原生的反射好像性能并不亚于hp，可能是jdk1.8对反射做了优化？？
 * Created by wangzhiyuan on 2018/8/16
 */
public class OnlyMethodInvokeTest {

    public void hello(){
        int sum = 0;
        for (int i = 0; i < 1000; i++) {
            sum += Math.sin(i);
        }
    }

    public static void main(String[] args) throws Exception {
        OnlyMethodInvokeTest instance = new OnlyMethodInvokeTest();
        String methodName = "hello";
        Class[] parameterTypes = null;
        Object[] parameters = null;

        MethodAccess methodAccess = MethodAccess.get(OnlyMethodInvokeTest.class);
        Method reflectMethod = instance.getClass().getMethod(methodName, parameterTypes);
        for (int i = 0; i < 1000; i++) {
            executeOnlyMethod(methodAccess, reflectMethod, instance, parameters);
            System.out.println("------------------------\n");
        }
    }

    private static void executeOnlyMethod(MethodAccess methodAccess, Method reflectMethod, OnlyMethodInvokeTest instance, Object[] parameters) throws Exception {
        final int circleTimes = 1000;

        long t0 = System.currentTimeMillis();
        for (int i = 0; i < circleTimes; i++) {
            instance.hello();
        }
        long t1 = System.currentTimeMillis();
        for (int i = 0; i < circleTimes; i++) {
            methodAccess.invoke(instance, "hello", parameters);
        }
        long t2 = System.currentTimeMillis();
        for (int i = 0; i < circleTimes; i++) {
            reflectMethod.invoke(instance, parameters);
        }
        long t3 = System.currentTimeMillis();

        long executeDirectly = t1 - t0;
        long executeByHp = t2 - t1;
        long executeByJdkReflect = t3 - t2;
        System.out.println("executeOnlyMethod ==> executeDirectly = [" + executeDirectly + "]ms");
        System.out.println("executeOnlyMethod ==> executeByHp = [" + executeByHp + "]ms");
        if (executeByJdkReflect <= executeByHp || executeByJdkReflect <= executeDirectly) {
            System.err.println("executeOnlyMethod ==> executeByJdkReflect = [" + executeByJdkReflect + "]ms");
        } else {
            System.out.println("executeOnlyMethod ==> executeByJdkReflect = [" + executeByJdkReflect + "]ms");
        }
    }
}
